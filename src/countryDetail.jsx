import { useEffect, useState, useContext } from "react";
import { useParams, Link } from "react-router-dom";
import { ThemeContext } from "./App";
import { InfinitySpin } from "react-loader-spinner";
import PropTypes from "prop-types";

const CountryDetail = (props) => {
  const { countryCode } = props;
  const { theme } = useContext(ThemeContext);
  const { id } = useParams();
  let [countryDetail, setCountryDetail] = useState([]);
  useEffect(() => {
    if (countryDetail.length === 0) {
      fetch(`https://restcountries.com/v3.1/alpha/${id}`)
        .then((response) => {
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          setCountryDetail(data);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
    }
  });
  console.log(countryCode);
  return (
    <>
      {countryDetail.length ? (
        <>
          <section className="back">
            <Link to="/">
              <button id={theme}><i className="fa-solid fa-arrow-left"></i>Back</button>
            </Link>
          </section>
          <div className="information">
            {countryDetail.map((country) => {
              return (
                <>
                  <img src={country.flags.png} className="flags" />
                  <div className="details">
                    <h2 className="countryName">{country.name.common}</h2>
                    <div className="grid">
                      <div className="grid-a">
                        <p>
                          <span className="title">Native Name: </span>
                          {country.name.nativeName
                            ? Object.keys(country.name.nativeName)
                                .map(
                                  (native) =>
                                    country.name.nativeName[native].common
                                )
                                .join(", ")
                            : "No Native Name Found"}
                        </p>
                        <p>
                          <span className="title">Population: </span>{" "}
                          {country.population.toLocaleString()}
                        </p>
                        <p>
                          <span className="title">Region: </span>
                          {country.region}
                        </p>
                        <p>
                          <span className="title">Sub-Region: </span>
                          {country.subregion}
                        </p>
                        <p>
                          <span className="title">Capital: </span>
                          {country.capital}
                        </p>
                      </div>
                      <div className="grid-b">
                        <p>
                          <span className="title">Top Level Domain: </span>
                          {country.tld}
                        </p>
                        <p>
                          <span className="title">Currencies:</span>
                          {country.currencies
                            ? Object.keys(country.currencies)
                                .map((cur) => {
                                  return country.currencies[cur].name;
                                })
                                .join(", ")
                            : "No currencies found "}
                        </p>
                        <p>
                          <span className="title">Languages: </span>
                          {country.languages
                            ? Object.keys(country.languages)
                                .map((lan) => {
                                  return country.languages[lan];
                                })
                                .join(", ")
                            : "No Languages found"}
                        </p>
                      </div>
                      <div className="grid-c">
                        <p>
                          <span className="title">Border Countries:</span>
                          {country.borders
                            ? Object.keys(country.borders)
                                .map((bor) => {
                                  return <button key={country.borders[bor]} id={theme}> {countryCode[country.borders[bor]]} </button>;
                                })
                            : "No border countries"}
                        </p>
                      </div>
                    </div>
                  </div>
                </>
              );
            })}
          </div>
        </>
      ) : (
        <div className="loading">
          <InfinitySpin
            visible={true}
            width="200"
            color="#4fa94d"
            ariaLabel="infinity-spin-loading"
          />
        </div>
      )}
    </>
  );
};
CountryDetail.propTypes = {
  countryCode: PropTypes.object.isRequired,
};

export default CountryDetail;
