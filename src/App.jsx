import "./index.css";
import "./nav.css";
import "./features.css";
import "./countryDetail.css";
import Features from "./features.jsx";
import Nav from "./nav.jsx";
import Error from "./error.jsx";
const url = "https://restcountries.com/v3.1/all";
import { useState, useEffect, createContext } from "react";
import { BrowserRouter,Route,Routes} from 'react-router-dom';
import CountryDetail from "./countryDetail.jsx";

export const ThemeContext = createContext({
  theme: "light",
  toggleTheme: () => {},
});

function App() {
  let [theme, setTheme] = useState("light");
  let [countriesData, setCountriesData] = useState([]);
  const toggleTheme = () => {
    setTheme((curr) => (curr === "light" ? "dark" : "light"));
  };
  useEffect(() => {
    if (countriesData.length === 0) {
      fetch(url)
        .then((response) => {
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          setCountriesData(data);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
          return (<><h2>No data found</h2></>)
        });
    }
  },);
  let countryCode=countriesData.reduce((acc,cur) => {
    
      acc[cur.cca3]=cur.name.common
      return acc
    }
,{})

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <BrowserRouter>
      <body>
        <Nav />
        <section className="countries" id={theme}>
        <Routes>
        <Route path="/" element={<Features countriesData={countriesData} setCountriesData={setCountriesData} />} />
        <Route path="/country/:id" element={<CountryDetail countryCode={countryCode}/>}/>
        <Route path="*" element={<Error />} /> 
        </Routes>
        </section>
        
      </body>
      </BrowserRouter>
    </ThemeContext.Provider>
  );
}

export default App;
