
import '@fortawesome/fontawesome-free/css/all.css';
import { useContext } from 'react';
import { ThemeContext } from './App';

const Nav = () => {
  const {theme,toggleTheme} =useContext(ThemeContext)
  return (<>

    <header className="nav" id={theme}>
       <h3 className="heading">Where in the world?</h3>
      <div className="theme" onClick={() => toggleTheme()}>
        <i className="fa-regular fa-moon"></i>
        <p className="mode" >Dark Mode</p>
      </div>
    </header>
    </>
  )
}

export default Nav
