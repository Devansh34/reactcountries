import PropTypes from "prop-types";
import { ThemeContext } from "./App";
import { useContext } from "react";


const Dropdown = ({ options, value, onChange, placeholder }) => {
    const { theme } = useContext(ThemeContext);
    return (
      <div className="dropdown" >
        <select value={value} onChange={onChange} id={theme}>
          <option value="All" className="">{placeholder}</option>
          {options.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
      </div>
    );
  };

  Dropdown.propTypes = {
    options: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
  };

  
  export default Dropdown;