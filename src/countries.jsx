import { useContext} from "react";
import { InfinitySpin } from 'react-loader-spinner'
import PropTypes from "prop-types";
import { ThemeContext } from "./App";
import { Link } from "react-router-dom";

const Countries = (props) => {
  const { filteredCountries,searchCountry} = props;
  const {theme} =useContext(ThemeContext)
  return (
    <>
      <div className="cardContainer">
        
        {filteredCountries.length?(filteredCountries.map((country) => {
          return (
            <Link key ={`${country.ccn3}`} to={`/country/${country.ccn3}`} style={{textDecoration:"none",color:"var(--Very-Dark-Blue-Light-mode)"}}>
            <div key={country.name.common} className="countryCard" id={theme}>
              <img src={country.flags.png} alt={country.name.common} />
              <p className="countryName">{country.name.common}</p>
              <ul>
                <li>
                  <span className="title">Population:</span>
                  {country.population.toLocaleString()}
                </li>
                <li>
                  <span className="title">Region:</span>
                  {country.region}
                </li>
                <li>
                  <span className="title">Capital:</span>
                  {country.capital}
                </li>
              </ul>
            </div>
            </Link>
          );
        })):(!searchCountry)?
        <div className="loading">
        <InfinitySpin
        visible={true}
        width="200"
        color="#4fa94d"
        ariaLabel="infinity-spin-loading"
        />
        </div> :<h2>No Country Found</h2>}
      </div>
    </>
  );
};

Countries.propTypes = {
  filteredCountries: PropTypes.array.isRequired,
  searchCountry:PropTypes.any
};

export default Countries;
