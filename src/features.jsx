import { useState, useContext } from "react";
import PropTypes from "prop-types";
import Countries from "./countries";
import { ThemeContext } from "./App";
import Dropdown from "./dropDown";

export default function Features(props) {
  const { theme } = useContext(ThemeContext);
  const { countriesData } = props;
  const [selectedRegion, setSelectedRegion] = useState("All");
  const [searchCountry, setSearchCountry] = useState("");
  const [selectedSubRegion, setSelectedSubRegion] = useState("All");
  const [selectedSortPopulation, setSelectedSortPopulation] = useState("All");
  const [selectedSortArea, setSelectedSortArea] = useState("All");
  let regionData = [];
  let subRegionData = [];

  const filtered = countriesData.filter((country) => {
    const nameIncludesSearch = country.name.common
      .toLowerCase()
      .includes(searchCountry);
    const regionMatches =
      selectedRegion === "All" || country.region === selectedRegion;
    const subRegionMatches =
      selectedSubRegion === "All" || country.subregion === selectedSubRegion;
    return nameIncludesSearch && regionMatches && subRegionMatches;
  });

  function handleSearch(e) {
    const searchValue = e.target.value.toLowerCase();
    setSearchCountry(searchValue);
    setSelectedSubRegion(selectedSubRegion);
  }

  function region() {
    countriesData.forEach((country) => {
      if (!regionData.includes(country.region)) {
        regionData.push(country.region);
      }
    });
  }
  region();
  function subRegion() {
    countriesData.forEach((item) => {
      if (item.region == selectedRegion) {
        if (!subRegionData.includes(item.subregion)) {
          subRegionData.push(item.subregion);
        }
      }
    });
  }

  subRegion();

  if (selectedSortPopulation == "ascending") {
    filtered.sort((a, b) => a.population - b.population);
  } else if (selectedSortPopulation == "descending") {
    filtered.sort((a, b) => b.population - a.population);
  }

  if (selectedSortArea == "ascending") {
    filtered.sort((a, b) => a.area - b.area);
  } else if (selectedSortArea == "descending") {
    filtered.sort((a, b) => b.area - a.area);
  }

  return (
    <>
      <div className="formId">
        <div className="search" id={theme}>
          <i className="fa-solid fa-magnifying-glass" id={theme}></i>
          <input
            type="text"
            name="search"
            placeholder="Search for a country..."
            id={theme}
            onChange={(e) => handleSearch(e)}
          />
        </div>
        
       <div className="drops"> 
        <Dropdown 
          options={["ascending", "descending"]}
          value={selectedSortArea}
          onChange={(e) => {
            setSelectedSortPopulation("All");
            setSelectedSortArea(e.target.value);
          }}
          placeholder="Sort by Area"
        />

        <Dropdown
          options={["ascending", "descending"]}
          value={selectedSortPopulation}
          onChange={(e) => {
            setSelectedSortPopulation(e.target.value);
            setSelectedSortArea("All");
          }}
          placeholder="Sort by Population"
        />

        <Dropdown
          options={subRegionData}
          value={selectedSubRegion}
          onChange={(e) => setSelectedSubRegion(e.target.value)}
          placeholder="Filter by Subregion"
        />
        <Dropdown
          options={regionData}
          value={selectedRegion}
          onChange={(e) => {
            setSelectedRegion(e.target.value);
            setSelectedSubRegion("All");
          }}
          placeholder="Filter by Region"
        />
      </div>
      </div>
      <Countries filteredCountries={filtered} searchCountry={searchCountry} />
    </>
  );
}

Features.propTypes = {
  countriesData: PropTypes.array.isRequired,
};
